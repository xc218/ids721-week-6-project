use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

#[derive(Deserialize)]
struct Request {
    school: Option<String>,
    student_program: Option<String>,
}

#[derive(Serialize)]
struct Response {
    name: String,
    student_id: Option<u64>,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;

    let client = Client::new(&config);

    let (student_name, student_id) = search_student(&client, request.school, request.student_program).await?;

    Ok(json!({
        "student_id": student_id,
        "name": student_name,
    }))
}

async fn search_student(client: &Client, school: Option<String>, student_program: Option<String>) -> Result<(String, Option<u64>), LambdaError> {
    let table_name = "studentInfo";

    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();

    if let Some(school_val) = school {

        expr_attr_values.insert(":school_val".to_string(), AttributeValue::S(school_val));

        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        filter_expression.push_str("school = :school_val");
    }

    if let Some(student_program_val) = student_program {
        
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        expr_attr_values.insert(":student_program_val".to_string(), AttributeValue::S(student_program_val));
        filter_expression.push_str("student_program = :student_program_val");
    }

    let result = client.scan()
        .table_name(table_name)
        .set_expression_attribute_values(Some(expr_attr_values))
        .set_filter_expression(Some(filter_expression))
        .send()
        .await?;

    let items = result.items.unwrap_or_default();

    let selected_item = items.iter().choose(&mut thread_rng());

    match selected_item {
        Some(item) => {
            let name_attr = item.get("name").and_then(|val| val.as_s().ok());
            let id_attr = item.get("student_id").and_then(|val| val.as_n().ok().map(|id| id.parse().ok()).flatten());

            match (name_attr, id_attr) {
                (Some(name), Some(id)) => Ok((name.to_string(), Some(id))),
                _ => Err(LambdaError::from("No name or student_id found in the selected item")),
            }
        },
        None => Err(LambdaError::from("No matching student information found")),
    }
}
