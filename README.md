# Ids721 Week 6 Project

## Project Description
Instrument a Rust Lambda Function with Logging and Tracing
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

This project is built on the foundation of Week 5 lambda function.

## Steps
1. Add dependencies to `Cargo.toml`
```
tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
```

2. Enable Monitoring Tools
- Enable X-Ray active tracing
- Enable Lambda Insights enhanced monitoring
![open](img/open.png)

3. Test the lambda function

## Results
1. X-Ray traces
![x](img/x-ray.png)

2. Cloudwatch traces
![x](img/log.png)